//dealer needs the information on the last car in their inventory.

function problem2(inventory){
    for(let i=0; i<=inventory.length;i++){
        if (inventory[i].id === inventory.length){
            return (`Last car is a ${inventory[i].car_make} ${inventory[i].car_model}`);
        }
    }
}

module.exports = problem2;
