//sorting the car_model in alphabetical order

function problem3(inventory,sortList,arr){
    sortList = inventory.sort((a, b) => (a.car_model > b.car_model) ? 1 : -1);

    for(let i in sortList){
        arr.push(sortList[i]["car_model"]);
    }
    return arr;
}

module.exports = problem3;