//To find out car id 33 

function problem1(inventory,input) {
    for(let i=0; i<=inventory.length; i++){
        if(inventory[i].id === input)
            return (`Car 33 is a ${inventory[i].car_year} ${inventory[i].car_make} ${inventory[i].car_model}`);
    }
}

module.exports = problem1;
