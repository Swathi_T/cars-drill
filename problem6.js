
//To find BMW and Audi cars within the inventory. 

function problem6(inventory,arr){
    for(let i in inventory){
        if (inventory[i]["car_make"] == 'BMW' || inventory[i]["car_make"] == 'Audi'){
            arr.push(inventory[i]);
        }
    }
    return arr;
}

module.exports = problem6